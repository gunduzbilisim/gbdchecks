# GBDchecks

Extension that contains functions and views currently using by GBD.

## IDEAS

- [ ] Function that help us to see large object privileges
- [ ] Function that help us to see type privileges
- [ ] Function that help us to see domain privileges
- [ ] Function that help us to see function, procedure, routine privileges

## INSTALLATION


1. If you're using CentOS, you have to install *"contrib" and "devel"* packages. *(devel package is used for pgxs, contrib package is used for pg_stat_statements)*
```bash
yum install postgresql11-contrib postgresql11-devel
```

2. This extension requires *"pg_stat_statements"* extension that comes with *"contrib"* package.
```sql
CREATE EXTENSION pg_stat_statements;
```

3. You must change or add *"shared_preload_libraries"* parameter in *"postgresql.conf"* like down below *(requires restart)*:
```bash
shared_preload_libraries = 'pg_stat_statements'
```

**INFO:** If you are not going to use *"check_queries"* view, then you don't need to restart the PostgreSQL.


4. For begin to install, you must clone the git repository:
```bash
git clone https://gitlab.com/huseynsnmz/gbdchecks.git
```

5. To copy files to proper locations with *pgxs*, change directory to *"gbdchecks"* and run this command:
```bash
make install
```

6. Then you can create the extension with this command:
```sql
CREATE EXTENSION gbdchecks;
```

**INFO:** If you want more users to use objects that created with this extension, then you can give privileges with these commands:
```bash
GRANT USAGE ON SCHEMA gbdchecks TO <KULLANICI>;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA gbdchecks TO <KULLANICI>;
GRANT SELECT ON ALL TABLES IN SCHEMA gbdchecks TO <KULLANICI>;
```


## INFORMATION

### HEALTH CHECK VIEWS


- View that help us to see slowest top 20 queries *(requires pg_stat_statements extension)*
```sql
SELECT * FROM gbdchecks.check_queries;
```

- View that help us to see bloat ratios and etc.:
```sql
SELECT * FROM gbdchecks.check_bloat;
```

- View that help us to see blocked queries:
```sql
SELECT * FROM gbdchecks.check_blocked_statements;
```

- View that help us to see hit ratio:
```sql
SELECT * FROM gbdchecks.check_hit_ratio;
```

- View that help us to see index sizes:
```sql
SELECT * FROM gbdchecks.check_index_sizes;
```

- View that help us to see index usage:
```sql
SELECT * FROM gbdchecks.check_index_usage;
```

- View that help us to see locks:
```sql
SELECT * FROM gbdchecks.check_locks;
```

- View that help us to see sequential scan values:
```sql
SELECT * FROM gbdchecks.check_seq_scans;
```

- View that help us to see unused indexes:
```sql
SELECT * FROM gbdchecks.check_unused_indexes;
```

- View that help us too see vacuum staticstics:
```sql
SELECT * FROM gbdchecks.check_vacuum_stats;
```



### PRIVILEGE CHECK VIEWS

- Function that help us to see *table* privileges of a user:
```sql
SELECT * FROM gbdchecks.table_privs('USERNAME');
```

- Function that help us to see *database* privileges of a user:
```sql
SELECT * FROM gbdchecks.database_privs('USERNAME');
```

* Function that help us to see *tablespace* privileges of a user:
```sql
SELECT * FROM gbdchecks.tablespace_privs('USERNAME');
```

- Function that help us to see *foreign data wrapper* permissions of a user:
```sql
SELECT * FROM gbdchecks.fdw_privs('USERNAME');
```

- Function that help us to see *foreign server* privileges of a user:
```sql
SELECT * FROM gbdchecks.fsrv_privs('USERNAME');
```

- Function that help us to see *language* privileges of a user:
```sql
SELECT * FROM gbdchecks.language_privs('USERNAME');
```

- Function that help us to see *schema* privileges of a user:
```sql
SELECT * FROM gbdchecks.schema_privs('USERNAME');
```

- Function that help us to see *view* privileges of a user:
```sql
SELECT * FROM gbdchecks.view_privs('USERNAME');
```

- Function that help us to see *sequence* privileges of a user:
```sql
SELECT * FROM gbdchecks.sequence_privs('USERNAME');
```

- Function that help us to see *all privileges* of a user:
```sql
SELECT * FROM gbdchecks.all_privs('USERNAME');
```

**INFO:** *Except domain, large object, type, function, procedure, routine privileges*
