# GBDchecks

GBD tarafindan kullanilan fonksiyon ve viewlarin bulundugu eklenti.

## FİKİRLER

- [ ] Large Object yetkilerini gösteren bir fonksiyon
- [ ] Type yetkilerini gösteren bir fonksiyon 
- [ ] Domain yetkilerini gösteren bir fonksiyon
- [ ] Function, Procedure, Routine yetkilerini gösteren bir fonksiyon

## KURULUM


1. Öncelikle eğer CentOS kullanıyorsanız *"contrib" ve "devel"* paketlerini kurmalısınız *(devel paketi pgxs'in kullanımı için, contrib paketi pg_stat_statements için gereklidir)*:
```bash
yum install postgresql11-contrib postgresql11-devel
```

2. Bu eklenti *"postgresql11-contrib"* paketi ile birlikte gelen *"pg_stat_statements"* eklentisinin kurulumunu gerektirir:
```sql
CREATE EXTENSION pg_stat_statements;
```

3. "postgresql.conf" dosyasındaki *"shared_preload_libraries"* bölümü aşağıdaki gibi değiştirilmeli veya eklenmelidir. *(restart gerektirir)*:
```bash
shared_preload_libraries = 'pg_stat_statements'
```

**BİLGİ:** *Eğer "check_queries" view'ını kullanmayacaksanız PostgreSQL'i yeniden başlatmadan eklentiyi kullanmaya devam edebilirsiniz.*


4. Kurulum yapmak için git deposunu klonlamalısınız:
```bash
git clone https://gitlab.com/huseynsnmz/gbdchecks.git
```

5. Dosyaların gerekli yerlere ulaşması için klonladığınız *"gbdchecks"* dizine girip aşağıdaki komutu çalıştırmalısınız:
```bash
make install
```

6. Eklentiyi aşağıdaki komut ile oluşturabilirsiniz:
```sql
CREATE EXTENSION gbdchecks;
```

**BİLGİ:** *Eğer birden fazla kullanıcının bu objeleri kullanmasını istiyorsanız aşağıdaki sorguları kullanabilirsiniz:*
```sql
GRANT USAGE ON SCHEMA gbdchecks TO <KULLANICI>;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA gbdchecks TO <KULLANICI>;
GRANT SELECT ON ALL TABLES IN SCHEMA gbdchecks TO <KULLANICI>;
```


## BILGILENDIRME

### KONTROL SORGULARI


- *En yavaş sorguları* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_queries;
```

- Sunucudaki *bloat oranlarını* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_bloat;
```

- *Engellenen sorguları* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_blocked_statements;
```

- *Hit ratio değerlerini* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_hit_ratio;
```

- *Index boyutlarını* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_index_sizes;
```

- *Index kullanımını* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_index_usage;
```

- *Kilitleri* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_locks;
```

- *Sequential scan değerlerini* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_seq_scans;
```

- *Kullanılmayan indexleri* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_unused_indexes;
```

- *Vacuum istatistiklerini* görüntüleyebileceğimiz view:
```sql
SELECT * FROM gbdchecks.check_vacuum_stats;
```



### YETKI KONTROL SORGULARI

- Bir kullanıcının *tablo* yetkilerini görüntüleyebileceğimiz fonksiyon:
```sql
SELECT * FROM gbdchecks.table_privs('KULLANICI_ADI');
```

- Bir kullanıcının *veritabanı* yetkilerini görüntüleyebileceğimiz fonksiyon:
```sql
SELECT * FROM gbdchecks.database_privs('KULLANICI_ADI');
```

- Bir kullanıcının *tablespace* yetkilerini görütülemek için gerekli fonksiyon
```sql
SELECT * FROM gbdchecks.tablespace_privs('KULLANICI_ADI');
```

- Bir kullanıcının *foreign data wrapper* yetkilerini görüntüleyebileceğimiz fonksiyon:
```sql
SELECT * FROM gbdchecks.fdw_privs('KULLANICI_ADI');
```

- Bir kullanıcının *foreign server* yetkilerini görüntüleyebileceğimiz fonksiyon:
```sql
SELECT * FROM gbdchecks.fsrv_privs('KULLANICI_ADI');
```

- Bir kullanıcının *language* yetkilerini görüntüleyebileceğimiz fonksiyon:
```sql
SELECT * FROM gbdchecks.language_privs('KULLANICI_ADI');
```

- Bir kullanıcının *şema* yetkilerini görüntüleyebileceğimiz fonksiyon:
```sql
SELECT * FROM gbdchecks.schema_privs('KULLANICI_ADI');
```

- Bir kullanıcının *view* yetkilerini görüntüleyebileceğimiz fonksiyon:
```sql
SELECT * FROM gbdchecks.view_privs('KULLANICI_ADI');
```

- Bir kullanıcının *sequence* yetkilerini ggörüntüleyebileceğimiz fonksiyon:
```sql
SELECT * FROM gbdchecks.sequence_privs('KULLANICI_ADI');
```

- Bir kullanıcının *tüm yetkilerini* görüntüleyebileceğimiz fonksiyon:
```sql
SELECT * FROM gbdchecks.all_privs('KULLANICI_ADI');
```

**BILGI:** *Except domain, large object, type, function, procedure, routine yetkileri hariç*
